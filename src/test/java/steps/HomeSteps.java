package steps;

import io.cucumber.java.en.Then;
import pages.HomeTabPage;
import pages.Page;

public class HomeSteps extends Page {

    @Then("^they see posts and information pertaining to the Home tab$")
    public void iSeePostsAndInformationPertainingToTheHomeTab() {
        instanceOf(HomeTabPage.class).assertIAmOnHomeTab();
    }
}